/*!
 * \authors Antonio Cassese <antonio.cassese@fi.infn.it>, INFN-Firenze
 * \date November 09 2020
 */

// Libraries
#include <boost/program_options.hpp> //!For command line arg parsing
#include <fstream>
#include <iomanip>
#include <iostream>
#include <limits>
#include <string>

#include "ITIVTools.h"

/*!
************************************************
* A simple "wait for input".
************************************************
*/
void wait()
{
    std::cout << "Press ENTER to continue...";
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}

/*!
************************************************
* Argument parser.
************************************************
*/
boost::program_options::variables_map process_program_options(const int argc, const char* const argv[])
{
    boost::program_options::options_description desc("Allowed options");

    desc.add_options()("help,h", "produce help message")

        ("config,c",
         boost::program_options::value<std::string>()->default_value("default"),
         "set configuration file path (default files defined for each test) "
         "...")

            ("verbose,v", boost::program_options::value<std::string>()->implicit_value("0"), "verbosity level")

                ("systemTestFlag,s", boost::program_options::value<std::string>()->implicit_value("0"), "flag to set tests using the chip"); // 0 = chip not connected

    boost::program_options::variables_map vm;
    try
    {
        boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
    }
    catch(boost::program_options::error const& e)
    {
        std::cerr << e.what() << '\n';
        exit(EXIT_FAILURE);
    }
    boost::program_options::notify(vm);

    // Help
    if(vm.count("help"))
    {
        std::cout << desc << "\n";
        exit(EXIT_SUCCESS);
    }

    return vm;
}

/*!
 ************************************************
 * Main.
 ************************************************
 */
int main(int argc, char* argv[])
{
    boost::program_options::variables_map v_map          = process_program_options(argc, argv);
    std::string                           configPath     = v_map.at("config").as<std::string>();
    bool                                  systemTestFlag = v_map.count("systemTestFlag");
    bool                                  verbose        = v_map.count("verbose");

    ITIVTools itIvTest = ITIVTools(configPath, systemTestFlag, verbose);
    itIvTest.RunIV();
    return 0;
}
