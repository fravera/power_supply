/*!
 * \authors Antonio Cassese <antonio.cassese@cern.ch>, INFN-Firenze
 * \date February 08 2023
 */

// Libraries
#include "DeviceHandler.h"
#include "Keithley.h"
#include "PowerSupply.h"
#include "PowerSupplyChannel.h"
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp> //!For command line arg parsing
#include <errno.h>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <limits>
#include <signal.h>
#include <stdio.h>

// Namespaces
namespace po = boost::program_options;

/*!
************************************************
* Ctrl-c to break the loop.
************************************************
*/
volatile sig_atomic_t stop;

void inthand(int signum) { stop = 1; }

/*!
************************************************
* A simple "wait for input".
************************************************
*/
void wait()
{
    std::cout << "Press ENTER to continue...";
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}

/*!
************************************************
* Save file opening.
************************************************
*/
FILE* fileOpen(std::string totalFileName)
{
    FILE* f_data = fopen((totalFileName + ".csv").c_str(), "w+");
    if(f_data == 0)
    {
        char              buffer[256];
        char*             errorMsg = strerror_r(errno, buffer, 256);
        std::stringstream error;
        error << "Error while opening " << totalFileName << ": " << errorMsg;
        throw std::runtime_error(error.str());
    }
    return f_data;
}

/*!
 ************************************************
 * Argument parser.
 ************************************************
 */
po::variables_map process_program_options(const int argc, const char* const argv[])
{
    po::options_description desc("Allowed options");

    desc.add_options()("help,h", "produce help message")

        ("config,c",
         po::value<std::string>()->default_value("default"),
         "set configuration file path (default files defined for each test) "
         "...")

            ("verbose,v", po::value<std::string>()->implicit_value("0"), "verbosity level");

    po::variables_map vm;
    try
    {
        po::store(po::parse_command_line(argc, argv, desc), vm);
    }
    catch(po::error const& e)
    {
        std::cerr << e.what() << '\n';
        exit(EXIT_FAILURE);
    }
    po::notify(vm);

    // Help
    if(vm.count("help"))
    {
        std::cout << desc << "\n";
        exit(EXIT_SUCCESS);
    }

    // Power supply object option
    if(vm.count("object")) { std::cout << "Object to initialize set to " << vm["object"].as<std::string>() << std::endl; }

    return vm;
}

/*!
 ************************************************
 * Main.
 ************************************************
 */
int main(int argc, char* argv[])
{
    boost::program_options::variables_map v_map = process_program_options(argc, argv);
    std::cout << "Initializing ..." << std::endl;

    std::string docPath = v_map["config"].as<std::string>();

    std::cout << "docPath: " << docPath << std::endl;

    // Taking configuration from xml file
    pugi::xml_document docSettings;

    DeviceHandler theHandler;
    theHandler.readSettings(docPath, docSettings);

    pugi::xml_node options = docSettings.child("Options");

    // Retriving options
    std::string folderName  = options.attribute("SaveFolder").value();
    std::string fileName    = options.attribute("FileName").value();
    std::string panelString = options.attribute("Panel").value();
    float       waitTime    = options.attribute("waitTimeSeconds").as_float();

    // Usefull variables
    float dummy   = 0;
    float current = 0;
    float voltage = 0;
    int   timeCnt = 0;
    int   timeSec = 0;

    // File handling variables
    FILE* f_data;

    // Directories and save file handling
    time_t     rawtime;
    struct tm* timeinfo;
    char       dateString[15];
    char       timeString[15];
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    strftime(dateString, sizeof(dateString), "%Y_%m_%d/", timeinfo);
    strftime(timeString, sizeof(timeString), "%H_%M_%S_", timeinfo);
    folderName += dateString;
    boost::filesystem::path dir(folderName.c_str());
    boost::filesystem::create_directories(dir);
    std::string totalFileName = folderName + timeString + fileName;
    std::string baseFileName  = timeString + fileName;

    // Power supply and power supply channel initialization
    PowerSupply*        powerSupply;
    PowerSupplyChannel* channelTmp;
    KeithleyChannel*    channel;
    // Printing out configuration
    std::cout << "************* Measurment settings summary *************" << std::endl;
    std::cout << "Wait time in seconds = " << waitTime << std::endl;
    std::cout << "panel = " << panelString << std::endl;
    std::cout << "totalFileName = " << totalFileName << std::endl;
    std::cout << "************************* END *************************" << std::endl;

    try
    {
        powerSupply = theHandler.getPowerSupply("MyKeithley");
        if(panelString == "front")
            channelTmp = powerSupply->getChannel("Front");
        else if(panelString == "rear")
            channelTmp = powerSupply->getChannel("Rear");
        else
        {
            std::stringstream error;
            error << "Panel " << panelString
                  << " is not valid, "
                     "please check the xml configuration file.";
            throw std::runtime_error(error.str());
        }
        channel = dynamic_cast<KeithleyChannel*>(channelTmp);
        f_data  = fileOpen(totalFileName); // Opening save file
        fprintf(f_data, "Time\tV\tI\n");

        std::cout << "Starting measurement" << std::endl;

        signal(SIGINT, inthand);
        while(!stop)
        {
            timeSec = timeCnt * waitTime;
            current = channel->getCurrent();
            voltage = channel->getOutputVoltage();
            fprintf(f_data, "%d\t%e\t%e\t", timeSec, voltage, current);
            fprintf(f_data, "\n");
            std::cout << timeSec << "\tVoltage: " << voltage << "\tCurrent: " << current << std::endl;
            sleep(waitTime);
            ++timeCnt;
        }

        std::cout << "Measurement done, putting keithley back in local mode" << std::endl;
        // std::cout << "Measurement done, press enter to continue" << std::endl;
        // getchar();

        channel->setParameter("command :system:local", dummy); // Putting commands in local mode
        fclose(f_data);
        std::cout << "Copying " + totalFileName + ".csv to " + folderName + "LastIV.csv" << std::endl;
        sleep(1);
        boost::filesystem::copy_file((totalFileName + ".csv").c_str(),                   // From
                                     (folderName + "LastIV.csv").c_str(),                // to
                                     boost::filesystem::copy_option::overwrite_if_exists // Copy options: overwrite if it exist
        );
    }
    catch(const std::out_of_range& oor)
    {
        std::cerr << "Out of Range error: " << oor.what() << '\n';
        throw std::out_of_range(oor.what());
    }
    return 0;
}
