/*!
 * \authors Pritam Palit <ppalit@cern.ch>, SINP, Kolkata
 * \authors Mattia Lizzo <mattia.lizzo@cern.ch>, INFN-Firenze
 * \authors Francesco Fiori <francesco.fiori@cern.ch>, INFN-Firenze
 * \authors Antonio Cassese <antonio.cassese@cern.ch>, INFN-Firenze
 * \date Sep 2 2019
 */

// Libraries
#include "DeviceHandler.h"
#include "PowerSupply.h"
#include "PowerSupplyChannel.h"
#include "IVCurveGeneralPowerSupply.h"
#include <boost/program_options.hpp> //!For command line arg parsing
#include <fstream>
#include <iomanip>
#include <iostream>
#include <limits>

// Namespaces
using namespace std;
namespace po = boost::program_options;

/*!
************************************************
* A simple "wait for input".
************************************************
*/
void wait()
{
    cout << "Press ENTER to continue...";
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
}

/*!
************************************************
* Argument parser.
************************************************
*/
po::variables_map process_program_options(const int argc, const char* const argv[])
{
    po::options_description desc("Allowed options");

    desc.add_options()("help,h", "produce help message")

        ("config,c",
         po::value<string>()->default_value("default"),
         "set configuration file path (default files defined for each test) "
         "...")("verbose,v", po::value<string>()->implicit_value("0"), "verbosity level");

    po::variables_map vm;
    try
    {
        po::store(po::parse_command_line(argc, argv, desc), vm);
    }
    catch(po::error const& e)
    {
        std::cerr << e.what() << '\n';
        exit(EXIT_FAILURE);
    }
    po::notify(vm);

    // Help
    if(vm.count("help"))
    {
        cout << desc << "\n";
        return 0;
    }

    // Power supply object option
    if(vm.count("object")) { cout << "Object to initialize set to " << vm["object"].as<string>() << endl; }

    return vm;
}

/*!
 ************************************************
 * Main.
 ************************************************
 */
int main(int argc, char* argv[])
{
    boost::program_options::variables_map v_map = process_program_options(argc, argv);
    std::cout << "Initializing ..." << std::endl;

    std::string        docPath = v_map["config"].as<string>();
    pugi::xml_document docSettings;

    DeviceHandler theHandler;

    theHandler.readSettings(docPath, docSettings);

    std::vector<std::string> Xmlparameter;
    
    //std::cout << "doc path : " << docPath << std::endl;

    //std::cout << "ID name : " << docSettings.child("Devices").child("PowerSupply").attribute("ID").value() << std::endl;

    std::map<std::string, std::string> powersupplyMap;
    for (pugi::xml_node child_PS: docSettings.child("Devices")) {
        for (pugi::xml_attribute attr_PS: child_PS.attributes()){
            //std::cout << "PS attr :  " << attr_PS.name() << "=" << attr_PS.value() << std::endl; 
            std::string attr_PS_name =  attr_PS.name();
            std::string attr_PS_value = attr_PS.value();
            powersupplyMap.insert(pair<std::string, std::string>(attr_PS_name, attr_PS_value) );
            
        }
    }

    std::cout << "PS Map to ID: " << powersupplyMap["ID"] << std::endl;  

    Xmlparameter.push_back(powersupplyMap["ID"]);

    std::multimap<std::string, std::string> channelMap;
    for (pugi::xml_node child_CH: docSettings.child("Devices").child("PowerSupply")) {
        for (pugi::xml_attribute attr_CH: child_CH.attributes())
        {  
            std::string attr_CH_name;
            std::string attr_CH_value;  
            attr_CH_name =  attr_CH.name() ;
            attr_CH_value =  attr_CH.value() ; 
            //channelMap[attr_CH_name] = attr_CH_value;
            channelMap.insert(std::make_pair(attr_CH_name, attr_CH_value));
        }
        
    } 

    typedef std::multimap<std::string, std::string>::iterator MMAPIterator;
    std::pair<MMAPIterator, MMAPIterator> result = channelMap.equal_range("ID");

    //std::cout << "CH Map size : " << channelMap.size() << std::endl;

    std::cout << "All values for key channel ID are," << std::endl;

    for (MMAPIterator it = result.first; it != result.second; it++)
    {
        std::cout << it->second << std::endl;
        Xmlparameter.push_back(it->second);
    }

    std::string csvfilename;
    std::string outputfolder = "./results";

    std::vector<std::string> outputnameextension;

    for (int i = 1 ; i <  Xmlparameter.size() ; i++){
        std::string PSName = Xmlparameter[0] + "_" + Xmlparameter[i];
        outputnameextension.push_back(PSName.c_str());
    }

    std::vector<PowerSupplyChannel> channelList;

    IVCurveGeneralPowerSupply theIVCurveGeneralPowerSupply(theHandler, Xmlparameter[0], Xmlparameter);
    theIVCurveGeneralPowerSupply.startReading();

    if (Xmlparameter[0] == "CAEN_SY5527")
    theIVCurveGeneralPowerSupply.scanIVCaen(docSettings, Xmlparameter[0], outputnameextension );
    else if (Xmlparameter[0] == "MyKeithley")
    theIVCurveGeneralPowerSupply.scanIVKeithley(docSettings, Xmlparameter[0], outputnameextension );
    
    theIVCurveGeneralPowerSupply.stopReading();

    theIVCurveGeneralPowerSupply.plotIVAverage(outputfolder, outputnameextension);
    wait();
            
            

    return 0;
}
