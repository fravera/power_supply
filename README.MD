# Project abstract

This project is meant to develop a generic c++ application to pilot several power supply types used for IT and OT testing.

# Doxygen documentation

Here is the link of the authomatically generated Doxygen main web page:

https://ancasses.web.cern.ch/ancasses/private/cms/DevicesCode/

# Clone instructions

Since April 2021 Networkutils submodule has been added. When you clone the repository don't forget it and please add the recursive submodule flag, i.e.:

```
git clone --recurse-submodules XXX
```

where XXX is your clone link.

If you already cloned the project and forgot the `--recurse-submodules`, you can run:

```
git submodule update --init --recursive
```

# Building instructions

## Prerequisite

In order to be able to compile you need the following libraries installed:
* **BOOST**. You can installed them from the repository:
```
sudo yum install boost-devel
```
* **PUGIXML** (https://pugixml.org/). You can installed them from the repository:
```
sudo yum install pugixml-devel
```

### CAEN prerequisite
In order to use the CAEN-flavour power supplies it is necessary to install the following libraries:
* **CAENHVWRAPPER** https://www.caen.it/download/?filter=CAEN%20HV%20Wrapper%20Library
* **CAENGeco** https://www.caen.it/download/?filter=GECO2020

## Building

* In the project folder create a subfolder "build" for example `mkdir build`
* Navigate to that `cd build`
* Run `cmake ../`
* Run `make` or `make -jN` where N is the number of core you would like to use for compiling

# Usage examples

Simple test examples can be found in the bin directory. Each of the provided examples has a dedicated help (run `testName --help`).
In order to establish the proper connection with the instrument(s) a configuration xml file has to be provided (and it's mandatory in all of the examples). Examples of configuration files can be found in the config folder.

After sourcing the setup with `source setup.sh` the bin folder is added to the $PATH environmental variable, thus the programs in bin can be executed everywhere and the path to the config file is relative to where you run the code.

## Interaction with Ph2ACF

The interaction with Ph2ACF happens via a TCP socket (ip 0.0.0.0, port 7000). The handling of the messages sent on the socket (headers and so on) makes use of the [NetworkUtils](https://gitlab.cern.ch/cms_tk_ph2/NetworkUtils) submodule, thus the version used in Ph2ACF and the library has to be the same.

The PowerSupplyController program starts the listening on the TCP socket and interprets the messages sent by Ph2ACF translating them into commands for the PS.
There are two ways of running the code:
* `PowerSupplyController -c configFile.xml`, with configFile.xml the configuration file with the list of needed instruments and the connection details
* `PowerSupplyController`, in this way the configuration file has to be provided somehow by Ph2ACF as well as the initialization of the instrument

Some details can be found in [this presentation](https://indico.cern.ch/event/1125589/contributions/4724671/attachments/2385889/4077704/PowerSupplyControllerUpdate.pdf).

## Other executables

### ITIVCurve

This executable uses configuration files like [iv_it_croc_sldo.xml](https://gitlab.cern.ch/cms_tk_ph2/power_supply/-/blob/main/config/iv_it_croc_sldo.xml), [iv_it_sldo.xml](https://gitlab.cern.ch/cms_tk_ph2/power_supply/-/blob/main/config/iv_it_sldo.xml) or [ivITsldo.xml](https://gitlab.cern.ch/cms_tk_ph2/power_supply/-/blob/main/config/ivITsldo.xml) and [iv_instruments_config.xml](https://gitlab.cern.ch/cms_tk_ph2/power_supply/-/blob/main/config/iv_instruments_config.xml) in the config folder, to acquire data for a low voltage SLDO IV curve and perform a simple analysis of the collected data (saved in cvs).

### IV_keithley_2410

Using configuration file, like the example in the config directory [iv_keithley_config.xml](https://gitlab.cern.ch/cms_tk_ph2/power_supply/-/blob/main/config/iv_keithley_config.xml), it performs an IV curve of sensors, saving data in cvs and possibly showing the plot.

### ResetITLVPower

**Untested**

Control of both HV and LV for switching on and off a detector (HV first, LV afterwards). Possible options:  switch first off then on; switch off only; switch on only.

# CLANG-FORMAT instructions

All code in this repo should be formatted according with CLANG-FORMAT.
CLANG included in yum is out of date and not compatible with this library.

## Installation from source

### Prerequisites

cmake3 and gcc10 are needed. They can be installed with:
```
sudo yum install cmake3
sudo yum install devtoolset-10
```

### Installation

Enable gcc10:
```
scl enable devtoolset-10 bash
```

Install from source:
```
git clone --depth=1 https://github.com/llvm/llvm-project.git
cd llvm-project
mkdir build
cd build/
cmake3 -DLLVM_ENABLE_PROJECTS=clang -DCMAKE_BUILD_TYPE=Release ../llvm
make
make install
```

## Usage

After sourcing the power supply library setup with
```
source setup.sh
```
by running
```
formatAll
```
all files should be updated with the correct format.
If you receive errors from terminal, CLANG was not installed properly, or your version is out of date.

## vim

There are many possibilities to integrate clang-format with your vim. Here following a list of tested and untested possibilities

### Tested

#### vim-autoformat

For a detailed view of the plugin, please refer to the original page: https://github.com/Chiel92/vim-autoformat .
Here following some basic instructions on how to install and use it:
1. Use you favourite PluginIn installer to install the plugin. For example:
    * *VAM* (https://github.com/MarcWeber/vim-addon-manager). Add this line to your .vimrc configuration file `VAMActivate git:https://github.com/Chiel92/vim-autoformat`
    * *vim-plug* (https://github.com/junegunn/vim-plug). Add the github repository (https://github.com/Chiel92/vim-autoformat) inside your call of vim plugins (usually between `call plug#begin('~/.vim/plugged')` and `call plug#end()`)
    * *Vundle* . Put this in your .vimrc `Plugin 'Chiel92/vim-autoformat'`
    * *Others using github*. Link to the vim/plug repository (https://github.com/Chiel92/vim-autoformat).
2. Close vim and reopen it in order to let him install the plugin.
3. Add to your .vimrc the wanted options:
    * `let g:autoformat_verbosemode=1`  suggested to have a verbose output on automatic format call. It helps for debugging.
    * `au BufWrite * :Autoformat` if you want Autoformat to be authomatically executed on save.
    * `noremap <F3> :Autoformat<CR>` if you want (for example) to have Autoformat executed with F3 button press. You can map to whatever else.
4. Beware of what formatter is used for your file and eventually change it using: the following syntax `let g:formatterpath = ['/some/path/to/a/folder', '/home/superman/formatters']` (more details on the original web page).

### Untested

* http://clang.llvm.org/docs/ClangFormat.html#vim-integration
* https://github.com/rhysd/vim-clang-format
* https://github.com/cjuniet/clang-format.vim

# Known issues

## Serial communicaion

When using serial communication the program has to be executed with root privileges (sudo ...). Alternatively, you should change the access privileges of your USB port.

### How to mount USB device with custom name

Using as reference https://unix.stackexchange.com/questions/66901/how-to-bind-usb-device-under-a-static-name you can do something like this:
* Edit the file /etc/udev/rules.d/10-local.rules with something like `ACTION=="add", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6001", SYMLINK+="myDevice"`, where
    * idVendor and idProduct can be found by running `udevadm info -a -p  $(udevadm info -q path -n /dev/ttyUSB0)` (`ttyUSB0` should be properly changed)
    * myDevice can be whatever name you choose and you device will appear under /dev/myDevice
* Reboot your system or execute `udevadm control --reload-rules && udevadm trigger`
