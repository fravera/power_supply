/*!
 * \authors Pritam Palit <ppalit@cern.ch>, SINP, Kolkata
 * \authors Fabio Ravera <fabio.ravera@cern.ch>, Fermilab
 * \authors Lorenzo Uplegger <uplegger@fnal.gov>, Fermilab
 * \date July 24 2020
 */


#include "IVCurveGeneralPowerSupply.h"
#include "DeviceHandler.h"
#include "Keithley.h"
#include "CAEN.h"
#include "PowerSupply.h"
#include "PowerSupplyChannel.h"
#include <fstream>
#include <iomanip>
#include <iostream>
#include <limits>
#include <string.h>
#include "TCanvas.h"
#include "TGraph.h"
#include "TFile.h"
#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include <bits/stdc++.h>

#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
using namespace std;

namespace po = boost::program_options;

int IVCurveGeneralPowerSupply::setPolarity(std::string pol_string)
{
    int polarity;
    if(pol_string == "positive")
        polarity = 1; // positive polarity
    else if(pol_string == "negative")
        polarity = -1; // negative polarity
    else
    {
        std::stringstream error;
        error << "polarity " << pol_string
              << " is not valid, "
                 "please check the xml configuration file.";
        throw std::runtime_error(error.str());
    }
    return polarity;
}

float IVCurveGeneralPowerSupply::getVRangeFromVmax(int vMax)
{
    float vRange;
    
    if(vMax > 1000)
    {
        std::stringstream error;
        error << "vMax " << vMax
              << " is not valid, "
                 "please check the xml configuration file.";
        throw std::runtime_error(error.str());
    }
    else if(vMax <= 0.2)
        vRange = 0.2;
    else if(vMax <= 20)
        vRange = 20;    
    else if(vMax <= 200)
        vRange = 200;
    else
        vRange = 1000;

    return vRange;
}

void IVCurveGeneralPowerSupply::preliminaryPSSettings(KeithleyChannel* channel)
{
    float dummy = 0;
    // channel->setParameter("command OUTP:SMOD NORM", dummy);              // Permits discharging of capacitive load when output is off
    // channel->setParameter("command :SYST:AZER ON", dummy);               // // Enables Auto-Zeroing of ADCs
    channel->setParameter("command :source:function volt", dummy);       // Configure source voltage
    channel->setParameter("command :source:volt:mode fixed", dummy);     // Set fixed voltage mode (aka not sweep)
    channel->setParameter("command :sense:function \'curr:dc\'", dummy); // Set measurement to DC current
    // channel->setParameter("command :func:conc on", dummy);               // Turn concurrent mode on, i.e. allow readout of voltage and current simultaneously
    channel->setParameter("command :sens:curr:nplc 10", dummy); // Set integration period to maximum (=10)
}

void IVCurveGeneralPowerSupply::printPresentVoltageAndCurrent(PowerSupplyChannel* psChannel)
{
    std::cout << "Present voltage is: " << psChannel->getSetVoltage() << " V\t current is: " << psChannel->getCurrent() << " A" << std::endl;
}

void IVCurveGeneralPowerSupply::wait()
{
    std::cout << "Press ENTER to continue...";
    std::cin.ignore(numeric_limits<streamsize>::max(), '\n');
}

void IVCurveGeneralPowerSupply::switchPSOff(PowerSupplyChannel* psChannel, float voltStep, float timeStep)
{
        if(!psChannel->isOn()) return;

        const int polarity = (psChannel->getSetVoltage() > 0) ? 1 : -1;
        voltStep *= polarity;
        float currentVoltage = psChannel->getSetVoltage();
 
        while(currentVoltage * polarity > fabs(voltStep))
        {
            currentVoltage -= voltStep;
            psChannel->setVoltage(currentVoltage);
            sleep(timeStep);
            currentVoltage = psChannel->getSetVoltage();
            printPresentVoltageAndCurrent(psChannel);

        }
    
        psChannel->setVoltage(0);
    
    //printPresentVoltageAndCurrent(psChannel);
    //psChannel->turnOff();
}

IVCurveGeneralPowerSupply::IVCurveGeneralPowerSupply(DeviceHandler& thehandler, std::string powersupplyName, std::vector<std::string> channelNameList)
{
    fPowersupply = thehandler.getPowerSupply(powersupplyName);
    fPowerSupplyName = powersupplyName;
    for (int i = 1; i < channelNameList.size() ; i++){
        //std::cout << "PS ID name : " << channelNameList[i] << std::endl;
        fChannelNameList.push_back(channelNameList[i].c_str()   );
    }
    configure();
}

IVCurveGeneralPowerSupply::~IVCurveGeneralPowerSupply() { std::cout << "--------------------------" << std::endl; }

void IVCurveGeneralPowerSupply::configure()
{
    std::cout << "Reading and starting the configuration " << std::endl;
    try
    {
        for (int i = 0; i < fChannelNameList.size() ; i++){
            std::cout << "fChannelNameList[i] : " << fChannelNameList[i] << std::endl;
            fChannelList.push_back(fPowersupply->getChannel(fChannelNameList[i]));
        }
    }
    catch(const std::out_of_range& oor)
    {
        std::cerr << "Out of Range error: " << oor.what() << '\n';
        throw std::out_of_range(oor.what());
    }

}

void IVCurveGeneralPowerSupply::WriteIVAfterRamp(PowerSupplyChannel* channel, float rampvoltage)
{
    std::cout << "Before ramping to : " << rampvoltage << " V,  \tIs on: " << channel->isOn() << "\tvoltage on channel 1: " << channel->getOutputVoltage()
              << "V\tcurrent on channel 1: " << channel->getCurrent() << std::endl;
    
    channel->rampToVoltage(rampvoltage);
    std::cout << "After ramping to : " << rampvoltage << " V, => \tIs on: " << channel->isOn() << "\tvoltage on channel 1: " << channel->getOutputVoltage()
              << "V\tcurrent on channel 1: " << channel->getCurrent() << std::endl;
    if(channel->getOutputVoltage() == rampvoltage) std::cout << "Channel is ramped up to " << rampvoltage << " V" << std::endl;
}

std::vector<float> IVCurveGeneralPowerSupply::rampvoltagevaluesvector(std::string str) {
    std::vector<float> v;
 
    std::stringstream ss(str);
 
    while (ss.good()) {
        std::string substr;
        getline(ss, substr, ',');
        v.push_back(stoi(substr));
    }
 
    return v;

}

void IVCurveGeneralPowerSupply::startReading()
{
    std::cout << "### Start turning the channels ON ###" << std::endl;


    for (int i = 0; i < fChannelList.size() ; i++){
        fChannelList[i]->turnOn();
        std::cout << "is Turn on : " << fChannelList[i]->isOn() << "\tCompliance voltage : " << fChannelList[i]->getVoltageCompliance() << "\tCompliance current : " << fChannelList[i]->getCurrentCompliance() << std::endl;
    }
    
}

void IVCurveGeneralPowerSupply::scanIVKeithley(pugi::xml_document& docSettings, std::string channelOption, std::vector<std::string> outputNameExtension)
{
    pugi::xml_node options = docSettings.child("Options");
    pugi::xml_node plot    = docSettings.child("Plot");

    // Retriving plot options
    bool        plotFlag      = plot.attribute("PlotFlag").as_bool();
    bool        saveFlag      = plot.attribute("SaveFlag").as_bool();
    std::string saveFormatStr = plot.attribute("SaveFormat").value();
    std::string folderName    = plot.attribute("SaveFolder").value();
    std::string fileName      = plot.attribute("FileName").value();
    std::string fileHeader    = plot.attribute("FileHeader").value();
    bool        mplFlag       = plot.attribute("MatplotlibFlag").as_bool();

    // Retriving measurement options
    float       voltStepSize         = options.attribute("VoltStepSize").as_float();
    float       voltStepSizeRampDown = options.attribute("VoltStepSizeRampDown").as_float();
    float       vMax                 = options.attribute("Vmax").as_float();
    float       waitTime             = options.attribute("waitTimeSeconds").as_float();
    float       compliance           = options.attribute("Compliance").as_float();
    float       currentReadRange     = options.attribute("CurrentReadRangeAmp").as_float();
    int         nPoints              = options.attribute("nPointsPerVolt").as_int();
    std::string pol_string           = options.attribute("Polarity").value();
    std::string panel_string         = options.attribute("Panel").value();

    // Usefull variables
    int   polarity;
    float dummy = 0;
    float vRange;

    std::vector<FILE*> f_data;
    std::vector<FILE*> f_data_avg;
    std::vector<std::string> outputfilename;

    float*     x = NULL;
    float*     y = NULL;
    float*     z = NULL;

    time_t     rawtime;
    struct tm* timeinfo;
    char       dateString[15];
    char       timeString[15];
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    strftime(dateString, sizeof(dateString), "%Y_%m_%d/", timeinfo);
    strftime(timeString, sizeof(timeString), "%H_%M_%S_", timeinfo);
    folderName += dateString;
    boost::filesystem::path dir(folderName.c_str());
    boost::filesystem::create_directories(dir);
    std::string totalFileName = folderName + timeString + fileName;

    for (int i = 0; i < fChannelList.size() ; i++){
        f_data_avg.push_back(fopen((folderName + "LastIV" + outputNameExtension[i] + "_average.csv").c_str(), "w+"));
        fCsvfileName.push_back((folderName + "LastIV" + outputNameExtension[i] + "_average.csv").c_str());
    }

    std::replace(fileHeader.begin(), fileHeader.end(), ',', '\t');

    // Printing out configuration
    std::cout << "************* Measurment settings summary *************" << std::endl;
    std::cout << "Polarity = " << pol_string << std::endl;
    std::cout << "Compliance = " << compliance << std::endl;
    std::cout << "Wait time in seconds = " << waitTime << std::endl;
    std::cout << "Step size in volts = " << voltStepSize << std::endl;
    std::cout << "Step size in volts for ramp down = " << voltStepSizeRampDown << std::endl;
    std::cout << "vMax = " << vMax << std::endl;
    std::cout << "panel = " << panel_string << std::endl;
    std::cout << "totalFileName = " << totalFileName << std::endl;

    std::cout << "Header = #" << fileHeader << std::endl;
    std::cout << "************************* END *************************" << std::endl;

    //fChannelVec  

    polarity = setPolarity(pol_string);
    vRange = getVRangeFromVmax(vMax);    

    try
    {
        std::vector<KeithleyChannel*> channelList;
        for (int i = 0; i < fChannelList.size() ; i++){
            channelList.push_back(dynamic_cast<KeithleyChannel*>(fChannelList[i]));
            switchPSOff(channelList[i], voltStepSizeRampDown, waitTime);
            channelList[i]->setCurrentCompliance(compliance);   // Set compliance
            channelList[i]->setParameter("Vsrc_range", vRange); // Configure voltage source range

            // configure current range auto                                           
            if(currentReadRange <= 0)
                channelList[i]->setParameter("autorange", true);
            else if(currentReadRange > 0)
            {
                channelList[i]->setParameter("autorange", false);
                channelList[i]->setParameter("command_arg :sens:curr:range", currentReadRange);
            }

            preliminaryPSSettings(channelList[i]);
        }
        
        cout << "---------------------------------------------------" << endl;

        
        for (int i = 0; i < fChannelList.size() ; i++){                  // Opening save file
        //fprintf(f_data[i], "#%s\n", fileHeader.c_str());
        fprintf(f_data_avg[i], "#%s\n", fileHeader.c_str());
        }        

        std::cout << "Starting measurement" << std::endl;

        int npoints = nPoints;
        fNPoints = vMax / voltStepSize;

        for(int i = 0; i < fNPoints; i++)
        {
            std::vector<float> voltage, current, currentSquare, stdCurrent;
            if(voltStepSize * (i + 1) * polarity > vRange) { std::cout << "Error, Voltage out of range" << std::endl; }
            //##if(voltStepSize * (i + 1) > vRange) { std::cout << "Error, Voltage out of range" << std::endl; }
            // loop over all channels
            for (int ivolt = 0; ivolt < fChannelList.size() ; ivolt++){
            channelList[ivolt]->setVoltage(voltStepSize * (i + 1));
            channelList[ivolt]->getCurrent();
            channelList[ivolt]->getOutputVoltage();
            voltage.push_back(0.); // added newly
            current.push_back(0.);
            currentSquare.push_back(0.);
            }

            sleep(waitTime);

            for(int vRead = 0; vRead < npoints; ++vRead)
            {
                sleep(0.1);
                //loop over all channels
                std::vector<float> currentTmp, voltageTmp;
                for (int ch = 0; ch < fChannelList.size() ; ch++){
                    //std::cout << "fCHannelList SIze : " << fChannelList.size() << std::endl;
                    currentTmp.push_back(channelList[ch]->getCurrent()* 1e+6);
                    voltageTmp.push_back(channelList[ch]->getOutputVoltage());
                    current[ch] += currentTmp[ch];
                    voltage[ch] += voltageTmp[ch];
                    currentSquare[ch] += pow(currentTmp[ch], 2);
                    std::cout << "Point " << vRead <<" Channel : " << fChannelNameList[ch] << " current: " << currentTmp[ch] << "\tvoltage: " << voltageTmp[ch] << std::endl;
                    //std::cout << "voltage for each channel in each iteration of a set : " << voltage[ch] << std::endl;
                    //fprintf(f_data[ch], "%e\t%e\t", voltageTmp[ch], currentTmp[ch]); // use the corresponding f_data of channel
                }
                
            }
            std::cout << "###END of ONE SET###" << std::endl;

            for (int ich = 0; ich < fChannelList.size() ; ich++){
            //fprintf(f_data[ich], "\n");
            current[ich] /= (float)npoints;
            voltage[ich] /= (float)npoints;
            //std::cout << "current after doing average : " << current[ich] << "\tnpoints : " << npoints << std::endl;
            //std::cout << "voltage after doing average : " << voltage[ich] << "\tnpoints : " << npoints << std::endl;

            stdCurrent.push_back(sqrtf((currentSquare[ich] / (float)npoints) - (pow(current[ich], 2))));
            fprintf(f_data_avg[ich], "%e\t%e\t%e\t%e\t", voltage[ich], current[ich], voltStepSize, stdCurrent[ich]); // insert into corresponding file to a channel
            fprintf(f_data_avg[ich], "\n");
            }

        }
        
        std::cout << "Measurement done, press enter to continue" << std::endl;

        getchar();

        for (int id = 0; id < fChannelList.size() ; id++){
            fclose(f_data_avg[id]);
        }

        // loop over all channels
        for (int iOut = 0; iOut < fChannelList.size() ; iOut++){
        switchPSOff(channelList[iOut], voltStepSizeRampDown, waitTime);  // Ramping down
        channelList[iOut]->setParameter("command :system:local", dummy); // Putting commands in local mode
        }
        sleep(1);
        
    
    }
    catch(const std::out_of_range& oor)
    {
        std::cerr << "Out of Range error: " << oor.what() << '\n';
        throw std::out_of_range(oor.what());
    }

}    

void IVCurveGeneralPowerSupply::scanIVCaen(pugi::xml_document& docSettings, std::string channelOption, std::vector<std::string> outputNameExtension)
{
    pugi::xml_node options = docSettings.child("Options");
    pugi::xml_node plot    = docSettings.child("Plot");

    // Retriving plot options
    bool        plotFlag      = plot.attribute("PlotFlag").as_bool();
    bool        saveFlag      = plot.attribute("SaveFlag").as_bool();
    std::string saveFormatStr = plot.attribute("SaveFormat").value();
    std::string folderName    = plot.attribute("SaveFolder").value();
    std::string fileName      = plot.attribute("FileName").value();
    std::string fileHeader    = plot.attribute("FileHeader").value();
    bool        mplFlag       = plot.attribute("MatplotlibFlag").as_bool();

    // Retriving measurement options
    float       voltStepSize         = options.attribute("VoltStepSize").as_float();
    float       voltStepSizeRampDown = options.attribute("VoltStepSizeRampDown").as_float();
    float       vMax                 = options.attribute("Vmax").as_float();
    float       waitTime             = options.attribute("waitTimeSeconds").as_float();
    float       compliance           = options.attribute("Compliance").as_float();
    float       currentReadRange     = options.attribute("CurrentReadRangeAmp").as_float();
    int         nPoints              = options.attribute("nPointsPerVolt").as_int();
    std::string pol_string           = options.attribute("Polarity").value();
    std::string panel_string         = options.attribute("Panel").value();

    // Usefull variables
    int   polarity;
    float dummy = 0;
    
    float vRange;

    std::vector<FILE*> f_data;
    std::vector<FILE*> f_data_avg;
    std::vector<std::string> outputfilename;

    float*     x = NULL;
    float*     y = NULL;
    float*     z = NULL;

    time_t     rawtime;
    struct tm* timeinfo;
    char       dateString[15];
    char       timeString[15];
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    strftime(dateString, sizeof(dateString), "%Y_%m_%d/", timeinfo);
    strftime(timeString, sizeof(timeString), "%H_%M_%S_", timeinfo);
    folderName += dateString;
    boost::filesystem::path dir(folderName.c_str());
    boost::filesystem::create_directories(dir);
    std::string totalFileName = folderName + timeString + fileName;

    for (int i = 0; i < fChannelList.size() ; i++){
        //##f_data.push_back(fopen((totalFileName + ".csv").c_str(), "w+"));
        f_data_avg.push_back(fopen((folderName + "LastIV" + outputNameExtension[i] + "_average.csv").c_str(), "w+"));
        fCsvfileName.push_back((folderName + "LastIV" + outputNameExtension[i] + "_average.csv").c_str());
    }

    std::replace(fileHeader.begin(), fileHeader.end(), ',', '\t');

    // Printing out configuration
    std::cout << "************* Measurment settings summary *************" << std::endl;
    std::cout << "Polarity = " << pol_string << std::endl;
    std::cout << "Compliance = " << compliance << std::endl;
    std::cout << "Wait time in seconds = " << waitTime << std::endl;
    std::cout << "Step size in volts = " << voltStepSize << std::endl;
    std::cout << "Step size in volts for ramp down = " << voltStepSizeRampDown << std::endl;
    std::cout << "vMax = " << vMax << std::endl;
    std::cout << "panel = " << panel_string << std::endl;
    std::cout << "totalFileName = " << totalFileName << std::endl;

    std::cout << "Header = #" << fileHeader << std::endl;
    std::cout << "************************* END *************************" << std::endl;

    vRange = getVRangeFromVmax(vMax);    

    try
    {
        std::vector<CAENChannel*> channelList;
        for (int i = 0; i < fChannelList.size() ; i++){
            channelList.push_back(dynamic_cast<CAENChannel*>(fChannelList[i]));
        }

        cout << "---------------------------------------------------" << endl;

        bool isBoardRamping = true;
        int  totChannelsRamping;
        while(isBoardRamping)
        {
            totChannelsRamping = 0;
            for (int i = 0; i < fChannelList.size() ; i++) totChannelsRamping += channelList[i]->isChannelRampingUp();
            if(!totChannelsRamping) isBoardRamping = false;
        }

        wait();
        cout << "---------------------------------------------------" << endl;

        
        for (int i = 0; i < fChannelList.size() ; i++){                  // Opening save file
            //##fprintf(f_data[i], "#%s\n", fileHeader.c_str());
            fprintf(f_data_avg[i], "#%s\n", fileHeader.c_str());
        }        

        std::cout << "Starting measurement" << std::endl;

        int npoints = nPoints;
        fNPoints = vMax / voltStepSize;
        
        for(int i = 0; i < fNPoints; i++)
        {
            std::vector<float> voltage, current, currentSquare, stdCurrent;
            //if(voltStepSize * (i + 1) * polarity > vRange) { std::cout << "Error, Voltage out of range" << std::endl; }
            if(voltStepSize * (i + 1) > vRange) { std::cout << "Error, Voltage out of range" << std::endl; }
            // loop over all channels
            for (int ivolt = 0; ivolt < fChannelList.size() ; ivolt++){
                channelList[ivolt]->setVoltage(voltStepSize * (i + 1));
                channelList[ivolt]->getCurrent();
                channelList[ivolt]->getOutputVoltage();
                voltage.push_back(0.);
                current.push_back(0);
                currentSquare.push_back(0);
            }

            sleep(waitTime);

            for(int vRead = 0; vRead < npoints; vRead++)
            {
                sleep(0.1);
                //loop over all channels
                std::vector<float> currentTmp, voltageTmp;
                for (int ch = 0; ch < fChannelList.size() ; ch++){
                    //std::cout << "fCHannelList SIze : " << fChannelList.size() << std::endl;
                    currentTmp.push_back(channelList[ch]->getCurrent());
                    voltageTmp.push_back(channelList[ch]->getOutputVoltage());
                    //std::cout << "current tmp : " << channelList[ch]->getCurrent() << "\tvoltage tmp : " << channelList[ch]->getVoltage() << std::endl;
                    current[ch] += currentTmp[ch];
                    voltage[ch] += voltageTmp[ch];
                    currentSquare[ch] += pow(currentTmp[ch], 2);
                    std::cout << "Point " << vRead <<" Channel : " << fChannelNameList[ch] << " current: " << currentTmp[ch] << "\tvoltage: " << voltageTmp[ch] << std::endl;
                    //##fprintf(f_data[ch], "%e\t%e\t", voltageTmp[ch], currentTmp[ch]); // use the corresponding f_data of channel
                }
            }
            std::cout << "###END of ONE SET###" << std::endl;
            //sleep(waitTime);

            for (int ich = 0; ich < fChannelList.size() ; ich++){
                //##fprintf(f_data[ich], "\n");
                //carefully loop over all channels
                current[ich] /= (float)npoints;
                voltage[ich] /= (float)npoints;
                stdCurrent.push_back(sqrtf((currentSquare[ich] / (float)npoints) - (pow(current[ich], 2))));
                fprintf(f_data_avg[ich], "%e\t%e\t%e\t%e\t", voltage[ich], current[ich], voltStepSize, stdCurrent[ich]); // insert into corresponding file to a channel
                //fprintf(stdout, "%e\t%e\t%e\t%e\t", voltage[ich], current[ich], voltStepSize, stdCurrent[ich]);
                //fprintf(stdout, "\n");
                //std::cout << "voltage avg : " <<  voltage[ich] << "\tcurrent avg : " << current[ich] << "\tstd Current : " << stdCurrent[ich] << std::endl;
                fprintf(f_data_avg[ich], "\n");
            }
            //sleep(waitTime);

        }
        
        std::cout << "Measurement done, press enter to continue" << std::endl;

        getchar();
        
        for (int id = 0; id < fChannelList.size() ; id++){
            fclose(f_data_avg[id]);
        }

        sleep(1);
        
        isBoardRamping = true;
        while(isBoardRamping)
        {
            totChannelsRamping = 0;
            for(int iCh = 0; iCh < fChannelList.size(); iCh++) totChannelsRamping += channelList[iCh]->isChannelRampingDown();
            if(!totChannelsRamping) isBoardRamping = false;
        }

        sleep(2);




    }
    catch(const std::out_of_range& oor)
    {
        std::cerr << "Out of Range error: " << oor.what() << '\n';
        throw std::out_of_range(oor.what());
    }

}    

void IVCurveGeneralPowerSupply::stopReading()
{
    std::cout << "### Start turning the channels OFF ###" << std::endl;
    for (int i = 0; i < fChannelList.size() ; i++){
    printPresentVoltageAndCurrent(fChannelList[i]);
    fChannelList[i]->turnOff();
    }
}

void IVCurveGeneralPowerSupply::plotIVAverage(std::string outputfolder, std::vector<std::string> outputNameExtension){

    std::cout << "##########Plotting Started###############" << std::endl;

    // https://stackoverflow.com/questions/33250380/c-skip-first-line-of-csv-file
    
    for (int i = 0; i < outputNameExtension.size() ; i++){
    std::vector<double> vec_data;  

    std::cout << "csv file name : " << fCsvfileName[i] << std::endl;

    int npoints = fNPoints;

    TCanvas *c = new TCanvas("c","c",600, 400);
    c->SetGrid();
    TGraphErrors *g;
    std::ifstream data(fCsvfileName[i], std::ifstream::binary);
    //std::ifstream data("./results/IV_data/2022_08_10/LastIVMyKeithley_Front_average.csv", std::ifstream::binary);
    //std::ifstream data("/home/modtest/ppalit/power_supply/results/IV_data/2022_06_02/LastIVMyKeithleyFront.csv");

    if (!data.is_open())
    {
        std::exit(EXIT_FAILURE);
    }
    std::string str;
    std::getline(data, str); // skip the first line

    double Vin[npoints], Iout[npoints], Vstep[npoints], Ierr[npoints];
    int nx = 0;
    while (std::getline(data, str))
    {
        std::istringstream iss(str);
        std::string token;
        //std::cout << "nx : " << nx << std::endl;
        //if (nx >= npoints) break;
        int ny = 0;
        while (std::getline(iss, token, '\t'))
        {   
            double token_val = std::stod(token);
            // process each token
            vec_data.push_back(token_val);
            ny++;     
        }
        //std::cout << "ny : " << ny << std::endl;
        //std::cout << "inside nx loop " << std::endl;
        Vin[nx] = vec_data.at(nx*ny);
        //std::cout << "voltage array : " << Vin[nx] << std::endl;  
        Iout[nx] = vec_data.at(nx*ny + 1);
        //std::cout << "current array : " << Iout[nx] << std::endl;
        Vstep[nx] = vec_data.at(nx*ny + 2);
        //std::cout << "voltage step array : " << Vstep[nx] << std::endl;
        Ierr[nx] = vec_data.at(nx*ny + 3);
        //std::cout << "current error array : " << Ierr[nx] << std::endl;
        
        nx++;
    }
    
    std::cout << "npoints : " << npoints << std::endl;

    std::string rootname = outputfolder + "/ivcurve_average_" + outputNameExtension[i] + ".root";

    TFile f(rootname.c_str(),"RECREATE");

    g = new TGraphErrors(npoints,Vin,Iout, Vstep, Ierr);
    g->SetMarkerColor(2);
    g->SetLineColor(2);
    g->SetMarkerStyle(2);
    std::cout << "end of one ix loop" << std::endl;

    g->Draw("ACP");
    g->SetTitle("I-V Curve; Voltage ( Volt ); Current ( #muA )");

    std::string pdfname = outputfolder + "/ivcurve_average_" + outputNameExtension[i] + ".pdf";
    g->Write();
    f.Write();
    c->SaveAs(pdfname.c_str());
    c->Update();
    //c->SaveAs(rootname.c_str());
    }

}

void IVCurveGeneralPowerSupply::plotIV(std::string outputfolder, int npoints){

    //TCanvas *c = new TCanvas("c","c",600, 400);


    // https://stackoverflow.com/questions/33250380/c-skip-first-line-of-csv-file
    
    //int npoints = 16;  
    
    for (int i = 0; i < fCsvfileName.size() ; i++){
    std::vector<double> vec_data;  

    TCanvas *c = new TCanvas("c","c",600, 400);
    TGraph *g[npoints];
    TMultiGraph *mg = new TMultiGraph(); 
    //TLegend *legend = new TLegend(0.44,0.57,0.87,0.87);
    for (int ix = 1; ix <= 2*npoints; ix = ix+2 ){
        std::ifstream data(fCsvfileName[i]);
        //std::ifstream data("/home/modtest/ppalit/power_supply/results/IV_data/2022_06_02/LastIVMyKeithleyFront.csv");

        if (!data.is_open())
        {
            std::exit(EXIT_FAILURE);
        }
        std::string str;
        std::getline(data, str); // skip the first line

        double Vin[npoints], Iout[npoints];
        int nx = 0;
        while (std::getline(data, str))
        {
            std::istringstream iss(str);
            std::string token;
            //std::cout << "nx : " << nx << std::endl;
            int ny = 0;
            while (std::getline(iss, token, '\t'))
            {   
                double token_val = std::stod(token);
                // process each token
                //std::cout << token_val << " ";
                vec_data.push_back(token_val);
                ny++;     
            }
            //std::cout << "ny : " << ny << std::endl;
            //std::cout << "inside nx loop " << std::endl;
            Vin[nx] = vec_data.at(nx*ny);
            //std::cout << "voltage array : " << Vin[nx] << std::endl;  
            Iout[nx] = vec_data.at(nx*ny + ix);
            //std::cout << "current array : " << Iout[nx] << std::endl;
            nx++;
        }
        
        g[(ix - 1)/2] = new TGraph(npoints,Vin,Iout);
        g[(ix - 1)/2]->SetMarkerColor((ix + 1)/2 + 1);
        g[(ix - 1)/2]->SetLineColor((ix + 1)/2 + 1);
        g[(ix - 1)/2]->SetMarkerStyle((ix + 1)/2 + 1);
        TString gr_title = "currentset " + std::to_string((ix - 1)/2);
        g[(ix - 1)/2]->SetTitle(gr_title);
        mg->Add(g[(ix-1)/2]);

        for (int i = npoints - 1; i >= 0; i--) std::cout << Vin[i] << "\t" << Iout[i] << std::endl;   
        //std::cout << "end of one ix loop" << std::endl;

    }

        mg->Draw("ALP");
        mg->SetTitle("IV curve");
        c->BuildLegend(0.83, 0.83, 0.92, 0.99, "", "APL");
        std::string pdfname = outputfolder + "/ivcurve.pdf";
        c->SaveAs(pdfname.c_str());
    }

}
