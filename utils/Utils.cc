#include "Utils.h"
#include <stdlib.h>
#include <iostream>

std::vector<float> createListOfSteps(float initialValue, float finalValue, float step)
{
    float currentSetVoltage = initialValue;
    if(currentSetVoltage == finalValue)
    {
        std::cout<< __PRETTY_FUNCTION__ << " [" << __LINE__ << "]" << std::endl;
        return std::vector<float>();
    }

    if((currentSetVoltage * finalValue) < 0)
    {
        std::cout<< "Current set voltage (" << currentSetVoltage << ") and new set voltage (" << finalValue << ") have opposite sign, operation not allowed" << std::endl;
        return std::vector<float>();
    }

    bool rampingUp = (abs(finalValue) > abs(currentSetVoltage));
    float sign = 0;
    if(finalValue != 0) sign = finalValue/abs(finalValue);
    else sign = currentSetVoltage/abs(currentSetVoltage);

    std::vector<float> listOfRampVoltages;
    bool targetReached = false;
    float lastAbsoluteVoltage = abs(currentSetVoltage);
    while(!targetReached)
    {
        float newAbsoluteVoltage;
        if(rampingUp)
        {
            newAbsoluteVoltage = lastAbsoluteVoltage + step;
            if(newAbsoluteVoltage >= abs(finalValue))
            {
                newAbsoluteVoltage = abs(finalValue);
                targetReached = true;
            }
        }
        else
        {
            newAbsoluteVoltage = lastAbsoluteVoltage - step;
            if(newAbsoluteVoltage <= abs(finalValue))
            {
                newAbsoluteVoltage = abs(finalValue);
                targetReached = true;
            }
        }
        listOfRampVoltages.push_back(sign*newAbsoluteVoltage);
        lastAbsoluteVoltage = newAbsoluteVoltage;
    }

    return listOfRampVoltages;
}