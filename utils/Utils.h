#ifndef __UTILS__
#define __UTILS__

#include <vector>

std::vector<float> createListOfSteps(float initialValue, float finalValue, float step);

#endif